#!/bin/sh
if [ $UID -ne 0 -a $USER != otrs ] ; then
  echo Run me as root or otrs
  exit 1
fi
OTRS=/opt/otrs
tar -cf - Kernel | tar -xvf - -C $OTRS
if [ $UID -eq 0 ] ; then
  chown -R otrs:www $OTRS/Kernel
fi
chmod -R g+w $OTRS/Kernel
$OTRS/bin/otrs.PackageManager.pl -a build -p JIRADemoPlugins.sopm
