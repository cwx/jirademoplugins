#  --
#  Kernel/System/TicketJIRADemo.pm - core modul
#  Copyright (C) 2013 catWorkX GmbH http://www.catworkx.de
#  --
#  This is commercial software.
#  Please contact catWorkX GmbH for a valid license.
#  --
package Kernel::System::TicketJIRADemo;

use strict;
use Data::Dumper;

sub mapOTRSOptionFieldDemoUser2JIRAAssignee {
	# This functions show how to simply change the JIRA's assignee by choosing an option in OTRS.
	# This function is meant to be used on the left side of the mapping table.
	# The usernames are taken from the preference value TicketJIRADemo###OTRS-JIRA-Demo-UserMapping in Core::SOAP.
	my ($TicketJIRAObj, $fieldmapping, %Ticket) = @_;
	my $DynamicField = $TicketJIRAObj->{DynamicFieldObject}->DynamicFieldGet( Name => 'DemoUser2JIRAAssignee' );
	my $ConfigObject = $TicketJIRAObj->{ConfigObject};
	my $demoUser = $TicketJIRAObj->{DynamicFieldValueObject}->ValueGet(
		FieldID  => $DynamicField->{ID},
		ObjectID => $Ticket{TicketID},
	);
	$TicketJIRAObj->{LogObject}->Log(
						Priority => 'debug',
						Message =>
							"Kernel::System::TicketJIRADemo::mapOTRSOptionFieldDemoUser2JIRAAssignee: demoUser: " . Dumper($demoUser) . "\n"
						);
	my $val = "";
	if ( $demoUser )  {
		$val = $demoUser->[0]->{ValueText};
		my $mapping = $ConfigObject->Get('TicketJIRADemo')->{'OTRS-JIRA-Demo-UserMapping'};
		$TicketJIRAObj->{LogObject}->Log(
						Priority => 'debug',
						Message =>
							"Kernel::System::TicketJIRADemo::mapOTRSOptionFieldDemoUser2JIRAAssignee: val: $val -- mapping: " . Dumper($mapping) . "\n"
						);
		if ( $mapping ) {
			if ( $mapping->{$val} ) {
				$val = $mapping->{$val};
			}
		}
	}
	return $val;
}

sub mapOTRSOptionToJIRAOption {
	# This functions show how to simply map a given OTRS dynamic field value (already in the HASH for JIRA) can be mapped to a different one in JIRA.
	# The values are taken from the preference value TicketJIRADemo###OTRS-JIRA-Demo-ValueMapping in Core::SOAP.
	# This function is meant to be used on the right side of the mapping table.
	my ($TicketJIRAObj, $val, $fields, %Ticket) = @_;
	my $ConfigObject = $TicketJIRAObj->{ConfigObject};
	my $mapping = $ConfigObject->Get('TicketJIRADemo')->{'OTRS-JIRA-Demo-ValueMapping'};
	$TicketJIRAObj->{LogObject}->Log(
						Priority => 'debug',
						Message =>
							"Kernel::System::TicketJIRADemo::mapOTRSOptionToJIRAOption: val: $val\n"
						);
	if ( $mapping ) {
		$TicketJIRAObj->{LogObject}->Log(
						Priority => 'debug',
						Message =>
							"Kernel::System::TicketJIRADemo::mapOTRSOptionToJIRAOption: mapping: " . Dumper($mapping) . "\n"
						);
		if ( $mapping->{$val} ) {
			$val = $mapping->{$val};
		}
	}
	$fields->{'custom_fields'}->{'color'} = $val;
	$TicketJIRAObj->{LogObject}->Log(
						Priority => 'debug',
						Message =>
							"Kernel::System::TicketJIRADemo::mapOTRSOptionToJIRAOption: fields: " . Dumper($fields) . "\n"
						);
	return 1;
}

1;